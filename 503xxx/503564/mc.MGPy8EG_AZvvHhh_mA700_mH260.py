from MadGraphControl.MadGraphUtils import *

# Generate A->ZH, H->bb, Z->ll or Z->vv

# parse the job arguments to get mA,mH and Z decay mode
phys_short = get_physics_short()
mA = float(phys_short.split('_')[2][2:])
mH = float(phys_short.split('_')[3][2:])
decay = (phys_short.split('_')[1][2:4])

print "mA ",mA
print "mH ",mH
print "Z decay ",decay

# Zll/Znunu
isZll=True 
if decay=='vv':
    isZll=False

print "isZll ",isZll

# a safe margin for the number of generated events
nevents=int(runArgs.maxEvents*1.4) 
mode=0 

# create the process string to be copied to proc_card_mg5.dat
process="""
import model 2HDM_GF
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~ a
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
# Define multiparticle labels
# Specify process(es) to run
"""
if isZll:
  process += """
generate     g g > h3 > h2 z, z > l- l+, (h2 > h1 h1)
output -f
"""
else:
  process += """
generate     g g > h3 > z h2 , z > vl vl~, (h2 > h1 h1)
output -f
"""

print 'process string: ',process

#---------------------------------------------------------------------------------------------------
# Set masses in param_card.dat
#---------------------------------------------------------------------------------------------------
mh1=125
mh2=mH
mh3=mA
print 'mh1,mh2,mh3 ',mh1,mh2,mh3
masses ={'25':mh1,  
         '35':mh2,
         '36':mh3}

decays ={'25':'DECAY 25 4.070000e-03 # Wh1',  
         '35':'DECAY 35 1.000000e-03 # Wh2',  
         '36':'DECAY 36 1.000000e-03 # Wh3'}
higgsmix={    '1 1':'  0.9553365   # TH1x1',
	      '1 2':'  0.2955202   # TH1x2',
	      '1 3':'  0.0000000   # TH1x3',
	      '2 1':' -0.2955202   # TH2x1',
	      '2 2':'  0.9553365   # TH2x2',
	      '2 3':'  0.0000000   # TH2x3',
	      '3 1':'  0.0000000   # TH3x1',
	      '3 2':'  0.0000000   # TH3x2',
	      '3 3':'  1.0000000   # TH3x3'}

params = {}
params['mass'] = masses
params['decay'] = decays
params['higgsmix'] = higgsmix

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#Fetch default LO run_card.dat and set parameters (extras)
extras = { 'lhe_version':'3.0', 
           'cut_decays':'F', 
           'pdlabel':"'lhapdf'",
           'lhaid':247000} # NNPDF23_lo_as_0130_qed
extras['nevents'] = nevents

# set up process
process_dir = new_process(process) 

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=params)
#build_param_card(param_card_old='param_card_AZH.dat',param_card_new='param_card_new.dat',params=params)
    
print_cards()

# and the generation
generate(process_dir=process_dir,grid_pack=False,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#### Pythia8 Showering with A14_NNPDF23LO
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
   
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
if isZll:
    evgenConfig.description = 'mass splitting A(%s GeV)->ZH(%s GeV)->llbbbb' % (mA,mH)
else:
    evgenConfig.description = 'mass splitting A(%s GeV)->ZH(%s GeV)->nunubbbb' % (mA,mH)

evgenConfig.keywords+=['BSMHiggs','bottom']
evgenConfig.contact = [ 'matthew.henry.klein@cern.ch' ]
#evgenConfig.inputfilecheck = ""

genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 5 -5' ]

