from MadGraphControl.MadGraphUtils import *
import subprocess
#from os.path import join as pathjoin 

fcard = open('proc_card_mg5.dat','w')

safefactor=1.1
nevents=runArgs.maxEvents*safefactor
runName='run_01'


# General settings                                                                                                          
nevents = runArgs.maxEvents*safefactor if runArgs.maxEvents>0 else safefactor*evgenConfig.nEventsPerJob

gridpack_mode=False

#==================================================================================

print "Generate ZH  events."


if not is_gen_from_gridpack():
    process = """
import model SMEFTsim_A_U35_MwScheme_UFO-massless_Hyy
define p = p b b~ 
generate p p > h l+ l- NP=1, h > a a NP=1 @0
add process p p > h ta+ ta- NP=1, h > a a NP=1 @1
add process p p > h vl vl~ NP=1, h > a a NP=1 @2
output -f
"""
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION




#==================================================================================



#==================================================================================

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0', 
           'cut_decays':'F', 
           'pdlabel':'lhapdf',
           'lhaid':'90400',
           'ickkw'       : 0,  
           'drll'        : 0.05,
           'drjj'        : 0.05,
           'drbb'        : 0.05,
           'drbj'        : 0.05,
           'draa'        : 0.00,
           'draj'        : 0.00,
           'ptgmin'      : 5.0,
           'r0gamma'     : 0.1,
           'xn'          : 2,
           'epsgamma'    : 0.1,
           'isoEM'       : "True",
           'ptb'         : 20.0,
           'ptj'         : 20.0,
           'ptl'         : 0.0,
           'etal'        : 10, 
           'ktdurham'    : 30,  
           'use_syst'    : "False",
           'nevents'    :int(nevents) 

}
 
   

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

#==================================================================================



#==================================================================================
#Building param_card setting to 0 c_is that are not of interest
 
frblock= {'cHW'   : 1.0, #28
          'cHB'   : 0.325, #30 
          'cHWB'  : 0.58, #32
          'cHl3'  : 0.0, #46
          'cHl1'  : 0.0, #45
          'cHq3'  : 0.0, #49
          'cHq1'  : 0.0, #48
          'cHu'   : 0.0, #50
          'cHd'   : 0.0, #51

          }
yukawa={'ymb'     : 0.0,
        }
params={}
params['frblock']=frblock
params['yukawa']=yukawa
modify_param_card(process_dir=process_dir,params=params)


generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#==================================================================================


rcard = open('reweight_card.dat','w')

params=["cHW","cHB","cHWB"]
block=[28,30,32]
vals=[-1.0,-0.5,-0.1,-0.05,0.05,0.1,0.5,1.0]

reweightCommand=""


for par in params:
    for val in vals:
        sign = "p"
        if val < 0.0:
            sign="m"
        valabs = str(abs(val)).replace(".","p")

        launch="launch --rwgt_name="+str(par)+"_"+sign+"_"+str(valabs)+"\n"
        reweightCommand=reweightCommand+launch
        idx=0
        for par2 in params:
            paramstring=""
            if(par2==par):
                paramstring="set frblock "+str(block[idx])+" "+str(val)+"\n"
            else:
                paramstring="set frblock "+str(block[idx])+" 0.0"+"\n"
            reweightCommand=reweightCommand+paramstring
            idx=idx+1

#This is SM                                        
                                                                        
SMprocess="change process p p > h l+ l-  NP=0, h > a a NP=0 \n"
SMprocess=SMprocess+"change process p p > h ta+ ta- NP=0, h > a a NP=0 --add \n"
SMprocess=SMprocess+"change process p p > h vl vl~ NP=0, h > a a NP=0 --add \n"
launch="launch --rwgt_name=SM"+"\n"
reweightCommand=reweightCommand+"\n"+SMprocess+launch


rcard.write(reweightCommand)
rcard.close()

subprocess.call('cp reweight_card.dat ' + process_dir+'/Cards/', shell=True)

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#==================================================================================
# Shower 
evgenConfig.description = 'MadGraphSMEFT_ZH_BSM'
evgenConfig.keywords    = ['VBF', 'Higgs', 'mH125', 'EFT']
evgenConfig.contact     = ['ana.cueto@cern.ch']
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']



include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")




