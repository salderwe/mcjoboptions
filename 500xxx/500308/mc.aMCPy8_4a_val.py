#--------------------------------------------------------------                                                                                               
# EVGEN configuration                                                                                                                                         
#--------------------------------------------------------------                                                                                               
evgenConfig.description = "MadGraph+Pythia8 samples for four-photon production at NLO"
evgenConfig.keywords = ["SM","3photon"]
evgenConfig.contact = ["amoroso@cern.ch","ana.cueto@cern.ch"]
evgenConfig.generators = ["aMcAtNlo","Pythia8"]
evgenConfig.nEventsPerJob = 10000

# --------------------------------------------------------------                                                                                              
# Generate events                                                                                                                                             
# --------------------------------------------------------------                                                                                              
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

gridpack_mode=True

if not is_gen_from_gridpack():
    process = """
    import model loop_sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > a a a a [QCD] @0
    output -f
    """
    
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

#Fetch default LO run_card.dat and set parameters
settings = {'parton_shower':'PYTHIA8', 
            'ptgmin':15,
            'etagamma':2.7,
            'R0gamma':0.1,
            'xn':2.0,
            'epsgamma':0.1,
            'isoEM':True,
            'req_acc':0.001,
            'nevents'      :int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

############################
# Shower JOs will go here


# --------------------------------------------------------------                                                                                              
# Shower settings                                                                                                                                             
# --------------------------------------------------------------                                                                                              
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")



genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]
