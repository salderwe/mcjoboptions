###############################
#
# WpWpWn to sslvlvjj
#
###############################
from MadGraphControl.MadGraphUtils import *

evgenConfig.nEventsPerJob = 20000
#nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

nevents = 60000

process = """
import model loop_sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define wpm = w+ w-
generate p p > w+ w+ w- [QCD]
output -f"""

#Fetch default NLO run_card.dat and set parameters
extras = { 'lhe_version'  :'2.0',
          'pdlabel'      :"'lhapdf'",
          'lhaid'        : 260000,
          'parton_shower':'PYTHIA8',
          'bwcutoff'     :'1000',
          'nevents'      :nevents
}

process_dir = new_process(process)
modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=extras)

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3)

# ############################
# # Shower JOs will go here
# #### Shower                    
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'MadGraph5_aMC@NLO_WpWpWn_sslvlvjj'
evgenConfig.keywords+=['triboson']
evgenConfig.contact = ['Ada Farilla <ada.farilla@cern.ch>']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

#--------------------------------------------------------------
# WpWpWn to sslvlvjj at Pythia8, Wp to leptons, Wn to quarks
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onPosIfAny=11 12 13 14 15 16',
                             '24:onNegIfAny =1 2 3 4 5']
###############################

evgenLog.info('2Lep6GeV filter is applied')

include ("GeneratorFilters/MultiElecMuTauFilter.py")
filtSeq.MultiElecMuTauFilter.MinPt  = 6000.
filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
filtSeq.MultiElecMuTauFilter.NLeptons = 2
filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0

filtSeq.Expression = "(MultiElecMuTauFilter)"

evt_multiplier = 10

if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevts=runArgs.maxEvents*evt_multiplier
    else:
        nevts=5000*evt_multiplier

