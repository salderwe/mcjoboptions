# Evgen parameters
evgenConfig.description = 'SPS production of associated J/psi+W with MadOnia, color-octet'
evgenConfig.keywords += ['Jpsi', 'W', 'QCD', 'Charmonium']
evgenConfig.contact = ['C. D. Burton <burton@utexas.edu>']
evgenConfig.generators += ['Lhef']

# Running Pythia
include('Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py')
include('Pythia8_i/Pythia8_LHEF.py')

# Choose decay modes for J/psi and W
genSeq.Pythia8.Commands += ['443:onMode = off']
genSeq.Pythia8.Commands += ['443:2:onMode = on']
genSeq.Pythia8.Commands += ['24:onMode = off']
genSeq.Pythia8.Commands += ['24:6:onMode = on']
genSeq.Pythia8.Commands += ['24:7:onMode = on']

# Filter the leptonic W decays
include('GeneratorFilters/OneLeptonFilter.py')
filtSeq.WZtoLeptonFilter.Ptcut_electron = 2e4
filtSeq.WZtoLeptonFilter.Ptcut_muon = 2e4

# Filter the di-muon J/psi decays
include('GeneratorFilters/DiLeptonMassFilter.py')
filtSeq.DiLeptonMassFilter.MinPt = 2000
filtSeq.DiLeptonMassFilter.MaxEta = 3
filtSeq.DiLeptonMassFilter.MinMass = 1900
filtSeq.DiLeptonMassFilter.MaxMass = 4600
filtSeq.DiLeptonMassFilter.AllowSameCharge = False

# Job parameters
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 2
