from MadGraphControl.MadGraphUtils import *
import os

# General settings
nevents=int(1.1*runArgs.maxEvents)
mode=0 #0 single machine, 1 cluster, 2 multicore
gridpack_dir='madevent/'
gridpack_mode=False
runName='run_01'

# MG Particle cuts
maxjetflavor=5
dyn_scale = '4' #-1 default, 1 Et, 2 mT, 3 mT/2, 4 \hat{s}

keyword=["ttbar", "BSM"]

runName = 'MGPy8.'+str(runArgs.jobConfig)[-8:-2]+'.clfv_Ql'

process1 = "generate p p > t t~ , ( t >  l+ l- lj ) , ( t~ >  w- b~, ( w- > l- vl~ ) ) "
process2 = "add process p p > t t~ , ( t >  w+ b , ( w+ > l+ vl )), ( t~ > l+ l- lj~ ) "

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model dim6top_CLFV_LO_UFO-Ql
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define lj = u c
define lj~ = u~ c~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
"""
+ process1 + "\n"
+ process2 +
"""
output -f
""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(grid_pack=gridpack_dir)

lhaid = 315000 #315000 = NNPDF31_lo_as_0118 90900 = PDF4LHC15 with 30 variations


extras = {'pdlabel'       : "'lhapdf'",
          'lhaid'       : lhaid,
          'lhe_version'   : '3.0',
          'maxjetflavor'  : maxjetflavor,
          'dynamical_scale_choice': dyn_scale,
          'use_syst'      : 'True',
          'sys_scalefact' : '0.5 1 2',
          'sys_alpsfact'  : '0.5 1 2',
          'sys_pdf'       : 'NNPDF31_lo_as_0118',
          'drjj'          : 0.0,
          'drll'          : 0.0,
          'draa'          : 0.0,
          'drjl'          : 0.0,
          'dral'          : 0.0,
          'ptl'           : 0.1,
          'pta'           : 0.0,
          'ptj'           : 0.1, # minimum jet pT
          'ptb'           : 0.1, # minimum jet pT
          'etaj'          : -1.0,
          'etab'          : -1.0,
          'etal'          : -1.0,
          'etaa'          : -1.0,}

build_run_card(
  run_card_old=get_default_runcard(proc_dir=process_dir),
  run_card_new='run_card.dat',
  nevts=nevents,
  rand_seed=runArgs.randomSeed,
  beamEnergy=beamEnergy,
  extras=extras)

print_cards()

generate(
  proc_dir=process_dir,
  run_card_loc='run_card.dat',
  param_card_loc=None,
  mode=mode,
  njobs=1,
  run_name=runName,
  grid_pack=gridpack_mode,
  gridpack_dir=gridpack_dir,
  nevents=nevents,
  random_seed=runArgs.randomSeed)

outDS = arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.tar.gz',lhe_version=3,saveProcDir=True)

# Go to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'You run with an athena MP-like whole-node setup. Re-configureing to run remainder of the job serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

evgenConfig.description = 'MGPy8_'
evgenConfig.keywords+=keyword
evgenConfig.description = 'ttbar_clfv'
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.contact = ["carlo.gottardo@cern.ch"]
runArgs.inputGeneratorFile = outDS

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
