mH = 125
mfd2 = 10
mfd1 = 2
mZd = 2000
nGamma = 2
avgtau = 175
decayMode = 'normal'
include("MadGraphControl_A14N23LO_FRVZdisplaced_zh.py")
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]
