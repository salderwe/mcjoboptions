import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':260000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[260000,90900], # list of pdfs ids for which all variations (error sets) will be included as weights
    'alternative_pdfs':None, # list of pdfs ids for which only the central set will be included as weights
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}


from MadGraphControl.MadGraphUtils import *

# General settings
nevents=int(1.1*runArgs.maxEvents)

defs = """
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define uc = u c
define uc~ = u~ c~
define ds = d s
define ds~ = d~ s~
"""

mcdec = defs+"""
generate p p > t t~ QCD=2 QED=0, (t > l+ vl b a), (t~ > ds uc~ b~)\n
add process p p > t t~ QCD=2 QED=0, (t > l+ vl b), (t~ > ds uc~ b~ a)\n
add process p p > t t~ QCD=2 QED=0, (t > uc ds~ b a), (t~ > l- vl~ b~)\n
add process p p > t t~ QCD=2 QED=0, (t > uc ds~ b), (t~ > l- vl~ b~ a)\n
add process p p > t t~ QCD=2 QED=0, (t > l+ vl b a), (t~ > l- vl~ b~)\n
add process p p > t t~ QCD=2 QED=0, (t > l+ vl b), (t~ > l- vl~ b~ a)
"""

process = """
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
"""+mcdec+"""
output -f
"""

settings = {'lhe_version'   :'3.0',
           'maxjetflavor'  :5,
           'cut_decays'    :'T',
           'ptl'           :20.,
           'ptgmin'        :15.,
           'R0gamma'       :0.1,
           'xn'            :2,
           'epsgamma'      :0.1,
           'ptj'           :1.,
           'xptl'          :20.,
           'etal'          :5.0,
           'etaa'          :5.0,
           'dynamical_scale_choice':'3',
           'nevents'    :nevents}

process_dir = new_process(process)
# Run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Print cards
print_cards()
# set up
generate(process_dir=process_dir,runArgs=runArgs)
# run
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)


# Go to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

## pythia shower
keyword=['SM','top', 'ttgamma', 'photon']
evgenConfig.keywords += keyword
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_ttgamma_nonallhad_GamFromDecay'
evgenConfig.contact = ["amartya.rej@cern.ch", "arpan.ghosal@cern.ch"]

runArgs.inputGeneratorFile=outputDS+".events"
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

