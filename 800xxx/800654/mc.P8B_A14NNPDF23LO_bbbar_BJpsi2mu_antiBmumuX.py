evgenConfig.description = "Pythia8B b b-bar -> B(mu mu X) anti-B(J/Psi->mu mu)"
evgenConfig.process = "b b-bar -> B(mu mu X) anti-B(J/Psi->mu mu)"
evgenConfig.keywords = ["4Muon","SM","heavyFlavour"]
evgenConfig.generators = ["Pythia8B"]
evgenConfig.contact  = ["yue.xu@cern.ch"]
evgenConfig.nEventsPerJob = 50

include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")
include("Pythia8B_i/Pythia8B_Photospp.py")

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True
#genSeq.Pythia8B.UserSelection = 'BJPSIINCLUSIVE'

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 9.']
genSeq.Pythia8B.QuarkPtCut = 6.0
genSeq.Pythia8B.AntiQuarkPtCut = 6.0
genSeq.Pythia8B.QuarkEtaCut = 3.0
genSeq.Pythia8B.AntiQuarkEtaCut = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 5  # 1 (old value)

genSeq.Pythia8B.Commands += ['541:onMode = off']
genSeq.Pythia8B.Commands += ['511:onMode = off']
genSeq.Pythia8B.Commands += ['521:onMode = off']
genSeq.Pythia8B.Commands += ['531:onMode = off']
genSeq.Pythia8B.Commands += ['5122:onMode = off']
genSeq.Pythia8B.Commands += ['5132:onMode = off']
genSeq.Pythia8B.Commands += ['5232:onMode = off']
genSeq.Pythia8B.Commands += ['5332:onMode = off']
###########B(mu mu X)
# Bc
genSeq.Pythia8B.Commands += ['541:onNegIfAny = 13']
genSeq.Pythia8B.Commands += ['541:onNegIfAny = 15']
# B0
genSeq.Pythia8B.Commands += ['511:onNegIfAny = 13']
genSeq.Pythia8B.Commands += ['511:onNegIfAny = 15']
# B+/-
genSeq.Pythia8B.Commands += ['521:onNegIfAny = 13']
genSeq.Pythia8B.Commands += ['521:onNegIfAny = 15']
# Bs
genSeq.Pythia8B.Commands += ['531:onNegIfAny = 13']
genSeq.Pythia8B.Commands += ['531:onNegIfAny = 15']
#LambdaB
genSeq.Pythia8B.Commands += ['5122:onNegIfAny = 13']
genSeq.Pythia8B.Commands += ['5122:onNegIfAny = 15']
#Xi_b+-
genSeq.Pythia8B.Commands += ['5132:onNegIfAny = 13']
genSeq.Pythia8B.Commands += ['5132:onNegIfAny = 15']
#Xi_b0
genSeq.Pythia8B.Commands += ['5232:onNegIfAny = 13']
genSeq.Pythia8B.Commands += ['5232:onNegIfAny = 15']
#Omega_b
genSeq.Pythia8B.Commands += ['5332:onNegIfAny = 13']
genSeq.Pythia8B.Commands += ['5332:onNegIfAny = 15']

#D0
genSeq.Pythia8B.Commands += ['421:onMode = off']
genSeq.Pythia8B.Commands += ['421:onIfAny = 13']
genSeq.Pythia8B.Commands += ['421:onIfAny = 15']
#D+/-
genSeq.Pythia8B.Commands += ['411:onMode = off']
genSeq.Pythia8B.Commands += ['411:onIfAny = 13']
genSeq.Pythia8B.Commands += ['411:onIfAny = 15']
#Ds
genSeq.Pythia8B.Commands += ['431:onMode = off']
genSeq.Pythia8B.Commands += ['431:onIfAny = 13']
genSeq.Pythia8B.Commands += ['431:onIfAny = 15']
###########anti-B(J/Psi->mu mu)
# B0
genSeq.Pythia8B.Commands += ['511:onPosIfAny = 443']
genSeq.Pythia8B.Commands += ['511:onPosIfAny = 100443']
genSeq.Pythia8B.Commands += ['511:onPosIfAny = 445']
genSeq.Pythia8B.Commands += ['511:onPosIfAny = 10441']
genSeq.Pythia8B.Commands += ['511:onPosIfAny = 10443']
genSeq.Pythia8B.Commands += ['511:onPosIfAny = 20443']
# B+/-
genSeq.Pythia8B.Commands += ['521:onPosIfAny = 443']
genSeq.Pythia8B.Commands += ['521:onPosIfAny = 100443']
genSeq.Pythia8B.Commands += ['521:onPosIfAny = 445']
genSeq.Pythia8B.Commands += ['521:onPosIfAny = 10441']
genSeq.Pythia8B.Commands += ['521:onPosIfAny = 10443']
genSeq.Pythia8B.Commands += ['521:onPosIfAny = 20443']
# Bs
genSeq.Pythia8B.Commands += ['531:onPosIfAny = 443']
genSeq.Pythia8B.Commands += ['531:onPosIfAny = 100443']
genSeq.Pythia8B.Commands += ['531:onPosIfAny = 445']
genSeq.Pythia8B.Commands += ['531:onPosIfAny = 10441']
genSeq.Pythia8B.Commands += ['531:onPosIfAny = 10443']
genSeq.Pythia8B.Commands += ['531:onPosIfAny = 20443']
# Bc
genSeq.Pythia8B.Commands += ['541:onPosIfAny = 443']
genSeq.Pythia8B.Commands += ['541:onPosIfAny = 100443']
genSeq.Pythia8B.Commands += ['541:onPosIfAny = 445']
genSeq.Pythia8B.Commands += ['541:onPosIfAny = 10441']
genSeq.Pythia8B.Commands += ['541:onPosIfAny = 10443']
genSeq.Pythia8B.Commands += ['541:onPosIfAny = 20443']
# LambdaB
genSeq.Pythia8B.Commands += ['5122:onPosIfAny = 443']
genSeq.Pythia8B.Commands += ['5122:onPosIfAny = 100443']
genSeq.Pythia8B.Commands += ['5122:onPosIfAny = 445']
genSeq.Pythia8B.Commands += ['5122:onPosIfAny = 10441']
genSeq.Pythia8B.Commands += ['5122:onPosIfAny = 10443']
genSeq.Pythia8B.Commands += ['5122:onPosIfAny = 20443']
# Xb+/-
genSeq.Pythia8B.Commands += ['5132:onPosIfAny = 443']
genSeq.Pythia8B.Commands += ['5132:onPosIfAny = 100443']
genSeq.Pythia8B.Commands += ['5132:onPosIfAny = 445']
genSeq.Pythia8B.Commands += ['5132:onPosIfAny = 10441']
genSeq.Pythia8B.Commands += ['5132:onPosIfAny = 10443']
genSeq.Pythia8B.Commands += ['5132:onPosIfAny = 20443']
# Xb
genSeq.Pythia8B.Commands += ['5232:onPosIfAny = 443']
genSeq.Pythia8B.Commands += ['5232:onPosIfAny = 100443']
genSeq.Pythia8B.Commands += ['5232:onPosIfAny = 445']
genSeq.Pythia8B.Commands += ['5232:onPosIfAny = 10441']
genSeq.Pythia8B.Commands += ['5232:onPosIfAny = 10443']
genSeq.Pythia8B.Commands += ['5232:onPosIfAny = 20443']
# Omega_b+/-
genSeq.Pythia8B.Commands += ['5332:onPosIfAny = 443']
genSeq.Pythia8B.Commands += ['5332:onPosIfAny = 100443']
genSeq.Pythia8B.Commands += ['5332:onPosIfAny = 445']
genSeq.Pythia8B.Commands += ['5332:onPosIfAny = 10441']
genSeq.Pythia8B.Commands += ['5332:onPosIfAny = 10443']
genSeq.Pythia8B.Commands += ['5332:onPosIfAny = 20443']

genSeq.Pythia8B.Commands += [
        "100443:onMode = off",
        "100443:onIfMatch = 11 11", # ee
        "100443:onIfMatch = 13 13", # mumu
        "100443:onIfMatch = 15 15", # tautau
        "100443:onIfAny = 443", # Jpsi
        "100443:onIfAny = 445", # chi_2c
        "100443:onIfAny = 10441", # chi_0c
        "100443:onIfAny = 10443", # h_1c
        "100443:onIfAny = 20443", # chi_1c
        "100443:0:bRatio = 0.01658", # ee
        "100443:1:bRatio = 0.1231", # mumu
        "100443:2:bRatio = 0.04722", # tautau
        "100443:21:bRatio = 0.0013", # Jpsi+pi0
        "100443:22:bRatio = 0.0324", # Jpsi+eta
        "100443:43:bRatio = 0.1658", # Jpsi+2pi0
        "100443:44:bRatio = 0.3366", # Jpsi+2pi
        "100443:23:bRatio = 0.0933", # chi_2c+gamma
        "100443:35:bRatio = 0.0922", # chi_0c+gamma
        "100443:36:bRatio = 0.0008", # h_1c+pi0
        "100443:37:bRatio = 0.0907"] # chi_1c+gamma
        
# Close all J/psi decays apart from J/psi->mu mu
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:onIfMatch = 13 13']
###################################

### Set lepton filters
genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [3.5, 2.5, 1.5] #GeV
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [2, 3, 4]

### Set lepton filters
if not hasattr(filtSeq, "M4MuIntervalFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import M4MuIntervalFilter
   M4MuIntervalfilter = M4MuIntervalFilter("M4MuIntervalfilter")
   filtSeq += M4MuIntervalfilter

filtSeq.M4MuIntervalfilter.MinPt = 1500 #MeV
filtSeq.M4MuIntervalfilter.MaxEta = 2.7
filtSeq.M4MuIntervalfilter.LowM4muProbability = 1.0
filtSeq.M4MuIntervalfilter.MediumMj4muProbability = 0.3
filtSeq.M4MuIntervalfilter.HighM4muProbability = 0.1
filtSeq.M4MuIntervalfilter.LowM4mu = 11000.
filtSeq.M4MuIntervalfilter.HighM4mu = 25000.
filtSeq.M4MuIntervalfilter.ApplyReWeighting = True

