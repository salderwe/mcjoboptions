evgenConfig.description = "Pythia8 jet photoproduction using nuclear photon flux and NCTEQ15 PDFs with A14 tune"
evgenConfig.keywords = ["QCD","coherent","jets"]
evgenConfig.contact = ["angerami@cern.ch"]
evgenConfig.nEventsPerJob   = 5000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# This AthenaTool will be used to set the Photon Flux
# =====================================================
# You can find the code in Generators/Pythia8_i/src/Pythia8Custom/
#  I included one property (Process) that can be set from the JobOptions
#  you can of course add others
from Pythia8_i.Pythia8_iConf import UserPhotonFlux
photonFluxTool=UserPhotonFlux()
photonFluxTool.Process=3
photonFluxTool.NuclearCharge=82;
photonFluxTool.MinimumB=6.62;


from AthenaCommon.AppMgr import ToolSvc
ToolSvc += UserPhotonFlux()
genSeq.Pythia8.CustomInterface = photonFluxTool

#options to set up gamma* p collisions
genSeq.Pythia8.Beam1='PROTON'
genSeq.Pythia8.Beam2='MUON'
genSeq.Pythia8.Commands += [
"Photon:Q2max = 1.0",
"Photon:ProcessType = 0",
"Photon:sampleQ2 = off",
"PDF:lepton2gamma = on",
"PDF:lepton2gammaSet = 2",
"PDF:lepton2gammaApprox = 2",
"MultipartonInteractions:pT0Ref = 3.0",
"PDF:gammaFluxApprox2xCut = 0.1",
]

#use NCTEQ nuclear PDFs
genSeq.Pythia8.Commands += ["PDF:pSet = LHAPDF6:nCTEQ15npFullNuc_208_82"]


#hard scattering details
genSeq.Pythia8.Commands +=["HardQCD:all = on",
                           "PhotonParton:all = on",
                           "PhaseSpace:pTHatMin = 15.0",
                           "Photon:Wmin  = 50.0"] #Wmin = 2 x pThatmin
MinPtForHTCalc = 8*GeV
MaxEtaForHTCalc = 4.4

#Set up truth jet finding
include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)

#Lower minimum pT in truth jet container
from AthenaCommon.AppMgr import ToolSvc
ToolSvc.AntiKt4TruthJetsFinder.PtMin=MinPtForHTCalc

#Use multijet filter to require at least two jets
if not hasattr( filtSeq, "QCDTruthMultiJetFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import QCDTruthMultiJetFilter
    filtSeq += QCDTruthMultiJetFilter()

filtSeq.QCDTruthMultiJetFilter.Njet = 2
filtSeq.QCDTruthMultiJetFilter.NjetMinPt = MinPtForHTCalc
filtSeq.QCDTruthMultiJetFilter.MaxEta = MaxEtaForHTCalc
filtSeq.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.QCDTruthMultiJetFilter.DoShape = False

#Now add actual HT filter
if not hasattr( filtSeq, "HTFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import HTFilter
    filtSeq += HTFilter()

filtSeq.HTFilter.MinJetPt = MinPtForHTCalc # Min pT to consider jet in HT
filtSeq.HTFilter.MaxJetEta = MaxEtaForHTCalc # Max eta to consider jet in HT
filtSeq.HTFilter.MinHT = 40.*GeV # Min HT to keep event
filtSeq.HTFilter.MaxHT = 120.*GeV # Max HT to keep event
filtSeq.HTFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.HTFilter.OutputLevel = 1


