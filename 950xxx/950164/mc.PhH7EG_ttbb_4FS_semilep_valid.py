evgenConfig.description = "POWHEG-BOX-RES/OpenLoops+Herwig7+EvtGen ttbb (4FS), mur=1/2*[mT(top)*mT(tbar)*mT(b)*mT(bbar)]**(1/4), muf=1/2*[mT(top)+mT(tbar)+mT(b)+mT(bbar)+mT(gluon)], semileptonic channel, hdamp=HT/2, H7.1-Default tune, decays with Powheg"
evgenConfig.keywords = [ 'SM', 'top', 'ttbar', 'bbbar', '1lepton']
evgenConfig.contact = ["lars.ferencz@cern.ch"]
evgenConfig.generators += [ 'Powheg' ]

#--------------------------------------------------------------
# Herwig7.1.3 H7.1-Default tune
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("Herwig7_i/Herwig71_AngularShowerScaleVariations.py")

# run Herwig7
Herwig7Config.run()
