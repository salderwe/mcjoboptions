#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 qq->H+Z+jet->vv minus [cbarc, bbarb] production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs"]
evgenConfig.contact     = [ 'stephen.jiggins@cern.ch' ]
evgenConfig.process = "ZH, H->[gg,qq,ll,ZZ,WW] off, Z->vv"
evgenConfig.inputFilesPerJob=200

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py")

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
#include('Pythia8_i/Pythia8_Powheg_Main31.py') # Not needed as it is included in AZNLO tune setup
genSeq.Pythia8.Commands += ['Powheg:NFinal = 3']


#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = on', # decay of Higgs
                             '25:offIfAny = 4 5' ] # Turn off for b\bar{b} & c\bar{c}
