#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with CMS settings for both Powheg and Pythia8.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar']
evgenConfig.contact     = [ 'm.fenton@cern.ch']
evgenConfig.generators  = [ 'Powheg', 'Pythia8' ]

#--------------------------------------------
# Powheg box b2 input from cms
#-------------------------------------------
include('PowhegControl/PowhegControl_tt_Common.py')
#                # ATLAS Keyword     # Powheg runcard keyword
PowhegConfig.__setattr__("PDF" , 306000) #lhans1 (+lhans2)
#PowhegConfig.__setattr__("qmass", 172.5) # setting this crashes the job, but default is correct anyway
#PowhegConfig.__setattr__("beam_energy", 6500) # ebeam1 + ebeam2
PowhegConfig.__setattr__("mu_F", 1) #facscfact
PowhegConfig.__setattr__("mu_R", 1) #renscfact

PowhegConfig.hdamp = 237.8775

if hasattr(PowhegConfig, "topdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.topdecaymode = 22222 # inclusive top decays
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "t t~ > all"

PowhegConfig.__setattr__("mass_W", 80.4) #tdec/wmass
PowhegConfig.__setattr__("width_W", 2.141) #tdec/wwidth
PowhegConfig.__setattr__("mass_b", 4.8) #tdec/bmass
PowhegConfig.__setattr__("width_t", 1.31) #tdec/twidth
PowhegConfig.__setattr__("BR_W_to_enu", 0.108) #tdec/elbranching
PowhegConfig.__setattr__("mass_e", 0.00051) #tdec/emass
PowhegConfig.__setattr__("mass_mu", 0.1057) #tdec/mumass
PowhegConfig.__setattr__("mass_tau", 1.777) #tdec/taumass
PowhegConfig.__setattr__("mass_d", 0.100) #tdec/dmass
PowhegConfig.__setattr__("mass_u", 0.100) #tdec/umass
PowhegConfig.__setattr__("mass_s", 0.200) #tdec/smass
PowhegConfig.__setattr__("mass_c", 1.5) #tdec/cmass
PowhegConfig.__setattr__("sin2cabibbo", 0.051) #tdec/sin2cabibbo
#PowhegConfig.__setattr__("use-old-grid", 1) 
#PowhegConfig.__setattr__("use-old-ubound", 1)
PowhegConfig.__setattr__("ncall1", 10000)
PowhegConfig.__setattr__("itmx1", 5)
PowhegConfig.__setattr__("ncall2", 100000)
PowhegConfig.__setattr__("itmx2", 5)
PowhegConfig.__setattr__("foldcsi", 1)
PowhegConfig.__setattr__("foldy", 1)
PowhegConfig.__setattr__("foldphi", 1)
PowhegConfig.__setattr__("nubound", 100000)
PowhegConfig.__setattr__("iymax", 1)
PowhegConfig.__setattr__("xupbound", 2)

PowhegConfig.generate()


from Pythia8_i.Pythia8_iConf import Pythia8_i
genSeq += Pythia8_i("Pythia8")
evgenConfig.generators += ["Pythia8"]
if "StoreLHE" in genSeq.Pythia8.__slots__.keys():
    print "Pythia8_Base_Fragment.py: DISABLING storage of LHE record in HepMC by default. Please reenable storeage if desired"
    genSeq.Pythia8.StoreLHE = False

genSeq.Pythia8.UseLHAPDF=False

assert hasattr(genSeq,"Pythia8")
genSeq.Pythia8.CollisionEnergy = int(runArgs.ecmEnergy)

#genSeq.Pythia8.Commands += [
#"Tune:ee = 7",
#"Tune:pp = 14",
#"PDF:useLHAPDF = on",
#"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
#]

#checked - matches cms (removed some related to uncertainty bands
genSeq.Pythia8.Commands += [ 'TimeShower:mMaxGamma = 1.0',
'6:m0 = 172.5']

genSeq.Pythia8.Commands += [
'Tune:pp 14',
'Tune:ee 7',
'MultipartonInteractions:ecmPow=0.03344',
'PDF:pSet=20',
'MultipartonInteractions:bProfile=2',
'MultipartonInteractions:pT0Ref=1.41',
'MultipartonInteractions:coreRadius=0.7634',
'MultipartonInteractions:coreFraction=0.63',
'ColourReconnection:range=5.176',
'SigmaTotal:zeroAXB=off',
'SpaceShower:alphaSorder=2',
'SpaceShower:alphaSvalue=0.118',
'SigmaProcess:alphaSvalue=0.118',
'SigmaProcess:alphaSorder=2',
'MultipartonInteractions:alphaSvalue=0.118',
'MultipartonInteractions:alphaSorder=2',
'TimeShower:alphaSorder=2',
'TimeShower:alphaSvalue=0.118']

#checked - matches CMS
genSeq.Pythia8.Commands += ['Tune:preferLHAPDF = 2',
'Main:timesAllowErrors = 10000',
'Check:epTolErr = 0.01',
'Beams:setProductionScalesFromLHEF = off',
'SLHA:keepSM = on',
'SLHA:minMassSM = 1000.',
'ParticleDecays:limitTau0 = on',
'ParticleDecays:tau0Max = 10',
'ParticleDecays:allowPhotonRadiation = on']
        
#checked - matches CMS        
genSeq.Pythia8.Commands += ['POWHEG:veto = 1',
'POWHEG:pTdef = 1',
'POWHEG:emitted = 0',
'POWHEG:pTemt = 0',
'POWHEG:pThard = 0',
'POWHEG:vetoCount = 100',
'SpaceShower:pTmaxMatch = 2',
'TimeShower:pTmaxMatch = 2']
 
include("Pythia8_i/Pythia8_Powheg_Main31.py")
#--------------------------------------------------------------                                                                                                                        \
                                                                                                                                                                                        
# Event filter                                                                                                                                                                         \
                                                                                                                                                                                        
#--------------------------------------------------------------                                                                                                                        \
                                                                                                                                                                                        
#include('GeneratorFilters/TTbarWToLeptonFilter.py')
#filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
#filtSeq.TTbarWToLeptonFilter.Ptcut = 0.


