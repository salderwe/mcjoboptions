#--------------------------------------------------------------
# Pythia8 showering setup
#--------------------------------------------------------------
# initialize Pythia8 generator configuration for showering

runArgs.inputGeneratorFile=runArgs.inputGeneratorFile

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

#--------------------------------------------------------------
# Edit merged LHE file to remove problematic lines
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg.py")

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["Powheg", "Pythia8"]
evgenConfig.description = "Non-resonant diHiggs production with cHHH=10 with Powheg-Box-V2, at NLO + full top mass, which decays to bbWW 2L."
evgenConfig.keywords = ["hh", "Higgs", "nonResonant", "bbWW", "bbll", "bottom", "lepton"]
evgenConfig.contact = ['Stefano Manzoni <Stefano.Manzoni@cern.ch>']
evgenConfig.nEventsPerJob = 10000
evgenConfig.maxeventsfactor = 1.0
evgenConfig.inputFilesPerJob = 4 # 10000 / 0.50 * 1.1 / 5500

#evgenConfig.tune = "MMHT2014"

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += ["25:onMode = off",
                            "25:oneChannel = 1 0.5 100 24 -24",
                            "25:addChannel = 1 0.5 100 5 -5",
                            "24:onMode = off",
                            "24:oneChannel = 1 0.425 100 11 -12",
                            "24:addChannel = 1 0.425 100 13 -14",
                            "24:addChannel = 1 0.150 100 15 -16",
                            "15:onMode = off",
                            "15:onIfAny = 11 13",
                            "24:mMin = 0", # W minimum mass
                            "24:mMax = 99999", # W maximum mass
                            "23:mMin = 0", # Z minimum mass
                            "23:mMax = 99999", # Z maximum mass
                            "TimeShower:mMaxGamma = 0" ]# Z/gamma* combination scale                      


#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("HWWFilter", PDGParent = [25], PDGChild = [24])

filtSeq.Expression = "HbbFilter and HWWFilter"
