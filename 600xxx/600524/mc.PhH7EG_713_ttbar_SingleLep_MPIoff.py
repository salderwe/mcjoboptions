# Provide config information
evgenConfig.generators    += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.tune           = "H7.1-Default"
evgenConfig.description    = "PowhegBox+Herwig7 7.1 ttbar production with Powheg hdamp equal top mass, H7.1-Default tune, exactly one lepton filter, with EvtGen - MPIoff"
evgenConfig.keywords       = ['SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact        = ['aknue@cern.ch']
evgenConfig.inputFilesPerJob=12
evgenConfig.nEventsPerJob=200
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# my Add: switch off MPI
Herwig7Config.add_commands(""" 
set /Herwig/Shower/ShowerHandler:MPIHandler    NULL 
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.


## JET FILTERING ##
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 1.0)

if not hasattr( filtSeq, "TruthJetFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
    filtSeq += TruthJetFilter()
    pass

filtSeq.TruthJetFilter.TruthJetContainer = "AntiKt10TruthJets"
filtSeq.TruthJetFilter.Njet = -1
filtSeq.TruthJetFilter.NjetMinPt = 500*GeV
filtSeq.TruthJetFilter.NjetMaxEta = 2.5
filtSeq.TruthJetFilter.jet_pt1 = 500*GeV
filtSeq.TruthJetFilter.applyDeltaPhiCut = False
filtSeq.TruthJetFilter.MinDeltaPhi = 0.2

filtSeq.Expression = "TTbarWToLeptonFilter and TruthJetFilter"
