#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+Z+jet-> gam G + l+l-  mG=40GeV
#--------------------------------------------------------------
evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 750

## Input dataset
## mc15_13TeV.345038.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Zincl_MINLO.evgen.TXT.e5590

#runArgs.inputGeneratorFile = '/eos/user/r/rmazini/datasets/mc15_13TeV.345038.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Zincl_MINLO.evgen.TXT.e5590'
 
#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
 
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
	genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']

else:
	genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]
#--------------------------------------------------------------
# H->yG decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '39:m0 = 40.0',                             
                             '25:oneChannel = 1 1. 100 22 39' ]

#--------------------------------------------------------------
# Lepon filter for Z->ll
#--------------------------------------------------------------
if not hasattr( filtSeq, "MultiLeptonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    filtSeq += MultiLeptonFilter()
    pass
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2
 
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet production: Z->ll, H->yG"
evgenConfig.keywords    = [ "BSM", "Higgs", "mH125" , "ZHiggs" , "Graviton" , "mG40" ]
evgenConfig.contact     = [ 'rachid.mazini@cern.ch' ]
evgenConfig.process = "qq->ZH, H->yG, Z->ll"
