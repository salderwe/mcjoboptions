from MadGraphControl.MadGraphUtils import *
import math
import os

if hasattr(runArgs, 'ecmEnergy'):
        beamEnergy = runArgs.ecmEnergy / 2.
else:
        raise RuntimeError("No center-of-mass energy provided in the Gen_tf.py command, exiting now.")

f_list = os.listdir(runArgs.jobConfig[0])
for i in f_list:
  if "mc" in os.path.splitext(i)[0] : 
    JO_RPV = i

joboptparts = JO_RPV.strip('\.py').split('_')

channel = joboptparts[3]

gentype = joboptparts[2]
decaytype = 'leplep'

run_card_extras = {}

runName = 'MGPy8.RPVSTau_%s' % (channel)

masses = float(joboptparts[-1].lstrip('m'))

LambdaLQD_111 = 0.11
LambdaLLE_112 = 0.07

MpWidth = (3 * LambdaLQD_111**2 + 2 * LambdaLLE_112**2) * masses / (16 * math.pi)

mgproc = None

if 'mutau' in channel:
    decays = """DECAY 1000016 """ + str(MpWidth) + """ # Width sneutrino Tau
##           BR         NDA      ID1       ID2
0.500000     2     13     -15    #BR(svt -> mu tau)
0.500000     2     -13     15    #BR(svt -> mu tau)
""" 
    mgproc="""
    generate p p > svt {decay1} @1
    add process p p > svt~ {decay2} @2
    """.format(decay1=", (svt  > mu ta)",decay2=", (svt~ > mu ta)")


elif 'etau' in channel:
    decays = """DECAY 1000016 """ + str(MpWidth) + """ # Width sneutrino Tau
##           BR         NDA      ID1       ID2
0.500000     2     11     -15    #BR(svt -> e tau)
0.500000     2     -11     15    #BR(svt -> e tau)
""" 
    mgproc="""
    generate p p > svt {decay1} @1
    add process p p > svt~ {decay2} @2
    """.format(decay1=", (svt  > e ta)",decay2=", (svt~ > e ta)")


elif 'emu' in channel:
    decays = """DECAY 1000016 """ + str(MpWidth) + """ # Width sneutrino Tau
##           BR         NDA      ID1       ID2
0.500000     2     11     -13    #BR(svt -> e mu)
0.500000     2     -11     13    #BR(svt -> e mu)
"""

    mgproc="""
    generate p p > svt {decay1} @1
    add process p p > svt~ {decay2} @2
    """.format(decay1=", (svt  > e mu)",decay2=", (svt~ > e mu)")

output = """
output -f
"""

process_str = """
import model RPVMSSM_UFO
define e = e- e+
define mu = mu- mu+
define ta = ta- ta+
"""+mgproc+output

m_description = None
if not 'tau' in channel:
    m_description = 'MadGraph+Pythia8+A14NNPDF23LO production dd~>svt>%s in RPV model, m_svt = %s GeV'%(channel,masses)
else:
    m_description = 'MadGraph+Pythia8+A14NNPDF23LO production dd~>svt>%s (tauhad filter) in RPV model, m_svt = %s GeV'%(channel,masses)

evgenConfig.description = m_description
evgenConfig.keywords += ['SUSY', 'RPV','sneutrino','resonance']
run_card_extras['lhaid'] = '247000' # NNDF23_lo_as_0130_qed pdf set
run_card_extras['pdlabel'] = 'nn23lo1'
run_card_extras['lhe_version'] = '2.0'
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

process_dir = new_process(process_str)

modify_run_card(run_card_input='run_card.dat', process_dir=process_dir, runArgs=runArgs, settings=run_card_extras)

param_card_extras = {}
param_card_extras['MASS'] = {'1000016':masses } # changing mass in the param_card
param_card_extras['DECAY'] = {'1000016':decays } # changing mass in the param_card

modify_param_card(param_card_input='param_card.SM.SVT.leplep.dat',process_dir=process_dir, params=param_card_extras)

generate(process_dir=process_dir, grid_pack=False, runArgs=runArgs)

arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=2, saveProcDir=True)

evgenConfig.contact = ['quanyin.li@cern.ch']


include("Pythia8_i/Pythia8_MadGraph.py")

#---------------#
# TauHad filter #
#---------------#
if 'tau' in channel:
    evgenLog.info('Apply Filter: 1 hadronic tau')
    from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
    filtSeq += MultiElecMuTauFilter("TauHadFilter")
    MultiElecMuTauFilter = filtSeq.TauHadFilter
    MultiElecMuTauFilter.NLeptons  = 1
    MultiElecMuTauFilter.MinPt = 1e10            # no light leptons will pass this pT threshold
    MultiElecMuTauFilter.MaxEta = 10.0
    MultiElecMuTauFilter.MinVisPtHadTau = 10000. # pt-cut on the visible hadronic tau
    MultiElecMuTauFilter.IncludeHadTaus = 1      # include hadronic taus
    #filtSeq.Expression = "(TauHadFilter)"
    evt_multiplier = 4
