from MadGraphControl.MadGraphUtilsHelpers import *
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

#Read the filename job options.
tokens = get_physics_short().split('_')

#Recover the masses from the parser.
mgluino = float(tokens[5])
decay_type = str(tokens[4])

#Configure the decays for the given model.
if decay_type == 'ALL':
  decays['1000021'] = """DECAY   1000021     1.00000000E+00   # gluino decays
  #       BR         NDA    ID1    ID2   ID3
    0.05555556        3      2      1     3
    0.05555556        3      2      1     5
    0.05555556        3      2      3     5
    0.05555556        3      4      1     3
    0.05555556        3      4      1     5
    0.05555556        3      4      3     5
    0.05555556        3      6      1     3
    0.05555556        3      6      1     5
    0.05555556        3      6      3     5
    0.05555556        3     -2     -1    -3
    0.05555556        3     -2     -1    -5
    0.05555556        3     -2     -3    -5
    0.05555556        3     -4     -1    -3
    0.05555556        3     -4     -1    -5
    0.05555556        3     -4     -3    -5
    0.05555556        3     -6     -1    -3
    0.05555556        3     -6     -1    -5
    0.05555556        3     -6     -3    -5
  """

if decay_type == 'UDB':
  decays['1000021'] = """DECAY   1000021     1.00000000E+00   # gluino decays
  #       BR         NDA    ID1    ID2   ID3
    0.50000000E+00    3      2      1     5     # BR(go -> u d b )
    0.50000000E+00    3     -2     -1    -5     # BR(go -> ~u ~d ~b )
  """

if decay_type == 'UDS':
  decays['1000021'] = """DECAY   1000021     1.00000000E+00   # gluino decays
  #       BR         NDA    ID1    ID2   ID3
    0.50000000E+00    3      2      1     3     # BR(go -> u d s )
    0.50000000E+00    3     -2     -1    -3     # BR(go -> ~u ~d ~s )
  """

#To perform the matching based on the number of jets in the process above.
njets = 2
masses['1000021'] = mgluino

#Define the process.
process = '''
import model RPVMSSM_UFO
define susysq = ul ur dl dr cl cr sl sr t1 t2 b1 b2
define susysq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate p p > go go QED=0 RPV=0 / susysq susysq~ @1
add process p p > go go j QED=0 RPV=0 / susysq susysq~ @2
add process p p > go go j j QED=0 RPV=0 / susysq susysq~ @3
'''

#Some useful configuration options.
evgenConfig.contact  = ["aaron.paul.o'neill@cern.ch"]
evgenConfig.keywords += [ 'SUSY', 'RPV', 'gluino', 'simplifiedModel']
evgenConfig.description = 'gluino pair production and decay in the UDD scenario to three quarks each (%s); six in total.  m_gluino = %s GeV'%(decay_type, mgluino)

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets > 0:
  genSeq.Pythia8.Commands += ["Merging:Process = pp>{go,1000021}{go,-1000021}"]
  genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
