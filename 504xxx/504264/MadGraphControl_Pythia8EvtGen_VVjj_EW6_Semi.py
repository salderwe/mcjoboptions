import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

import os, shutil, copy
### get MC job-options filename
FIRST_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
jofiles = [f for f in os.listdir(FIRST_DIR) if (f.startswith('mc') and f.endswith('.py'))]

mode=0 # 0: single core; 1: cluster; 2: multicore
cluster_type="condor"
cluster_queue=None
if mode!=1:
  cluster_type=None
  cluster_queue=None
gridpack_mode=True

genproc=""
VVname=""
if "WZ" in procVVjj:
  VVname="WZ"
  if "llqq" in procVVjj:
    genproc="""
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define w = w+ w-
    generate p p > j j w z QCD=0 QED=4, z > l+ l-, w > j j
    """
  elif "lvqq" in procVVjj:
    genproc="""
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > j j w+ z QCD=0 QED=4, w+ > l+ vl, z > j j
    add process p p > j j w- z QCD=0 QED=4, w- > l- vl~, z > j j
    """
  elif "vvqq" in procVVjj:
    genproc="""
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define w = w+ w-
    generate p p > j j w z QCD=0 QED=4, z > vl vl~, w > j j
    """
  else:
    raise RuntimeError("Unsupported process for WZ: %s." % procVVjj)

elif "WW" in procVVjj:
  VVname="WW"
  if "lvqq" in procVVjj:
    genproc="""
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > j j w+ w- QCD=0 QED=4, w+ > l+ vl, w- > j j
    add process p p > j j w+ w- QCD=0 QED=4, w- > l- vl~, w+ > j j
    add process p p > j j w+ w+ QCD=0 QED=4, w+ > l+ vl, w+ > j j
    add process p p > j j w- w- QCD=0 QED=4, w- > l- vl~, w- > j j
    """
  else:
    raise RuntimeError("Unsupported process for WW: %s." % procVVjj)

elif "ZZ" in procVVjj:
  VVname="ZZ"
  if "llqq" in procVVjj:
    genproc="""
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > j j z z QCD=0 QED=4, z > l+ l-, z > j j
    """
  elif "vvqq" in procVVjj:
    genproc="""
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > j j z z QCD=0 QED=4, z > vl vl~, z > j j
    """
  else:
    raise RuntimeError("Unsupported process for ZZ: %s." % procVVjj)

else:
    raise RuntimeError("Unsupported process: %s." % procVVjj)

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
if not is_gen_from_gridpack():
    process = """
    import model sm
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    %s
    output -f
    """ % (genproc)

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION


#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
## safe factor applied to nevents, to account for the filter efficiency
nevents = runArgs.maxEvents*safefactor if runArgs.maxEvents>0 else safefactor*evgenConfig.nEventsPerJob
nevents = int(nevents)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {
    'asrwgtflavor':"5",
    'lhe_version':"3.0",
    'ptj':"15",
    'ptb':"15",
    'pta':"0",
    'ptl':"4",
    'misset':"10",
    'etaj':"5",
    'etab':"5",
    'etal':"2.8",
    'drjj':"0",
    'drll':"0",
    'draa':"0",
    'draj':"0",
    'drjl':"0",
    'dral':"0",
    'mmjj':"10",
    'mmbb':"10",
    'mmll':"40",
    'maxjetflavor':"5" ,
    'cut_decays'  :'T',
    'auto_ptj_mjj': 'F',
    'nevents'     : nevents,
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

print_cards()

#---------------------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Helper for resetting process number
check_reset_proc_number(opts)

#### Shower

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Dipole shower
genSeq.Pythia8.Commands += [
            "SpaceShower:dipoleRecoil = on"]

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = "MadGraph %s plus two EWK jets" % procVVjj
evgenConfig.keywords+=['SM',VVname,'2jet','VBS']

evgenConfig.contact = ['Lailin Xu <lailin.xu@cern.ch>', 'Dimitris Varouchas <dimitris.varouchas@cern.ch>']

