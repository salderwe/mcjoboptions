evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 410450 LHE files with Shower Weights added, Matrix Element correction off, global recoil on, all-hadronic and HT filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'allhadronic']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch','mdshapiro@lbl.gov','khoo@cern.ch']
evgenConfig.nEventsPerJob = 500
evgenConfig.inputFilesPerJob = 10 

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#aMCatNLO-like settings
genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch = 1",
                            "SpaceShower:pTmaxFudge = 1",
                            "SpaceShower:MEcorrections = off",
                            "TimeShower:pTmaxMatch = 1",
                            "TimeShower:pTmaxFudge = 1",
                            "TimeShower:MEcorrections = off",
                            "TimeShower:globalRecoil = on",
                            "TimeShower:weightGluonToQuark=1."
                            ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
#all-hadronic filter
include("GeneratorFilters/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = 0
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

#HT filter
include("GeneratorFilters/HTFilter.py")
filtSeq.HTFilter.MinJetPt = 35.*GeV # Min pT to consider jet in HT
filtSeq.HTFilter.MaxJetEta = 2.5 # Max eta to consider jet in HT
filtSeq.HTFilter.MinHT = 1000.*GeV # Min HT to keep event
filtSeq.HTFilter.MaxHT = 1500.*GeV # Max HT to keep event
# EvtGen changes the B decays, which can cause inconsistencies
# between generator-level and DAOD-level filtering
filtSeq.HTFilter.UseNeutrinosFromWZTau = True # Include nu from the MC event in the HT
# These should be irrelevant for the sample, but kept for consistency
filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
filtSeq.HTFilter.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
filtSeq.HTFilter.MaxLeptonEta = 2.5 # Max eta to consider muon in HT

filtSeq.Expression = "(TTbarWToLeptonFilter and HTFilter)"
