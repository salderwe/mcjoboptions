#process name, for inputfilecheck
stringy="FS0_50_ZZjj_llqq"

#--------------------------------------------------------------------------------------------------------------------
# multi-core cleanup
#--------------------------------------------------------------------------------------------------------------------
# multi-core running, if allowed!
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts


#--------------------------------------------------------------------------------------------------------------------
# Shower
#--------------------------------------------------------------------------------------------------------------------

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

include("GeneratorFilters/FourLeptonMassFilter.py")
filtSeq.FourLeptonMassFilter.MinPt = 4000.
filtSeq.FourLeptonMassFilter.MaxEta = 4.
filtSeq.FourLeptonMassFilter.MinMass1 = 40000.
filtSeq.FourLeptonMassFilter.MaxMass1 = 14000000.
filtSeq.FourLeptonMassFilter.MinMass2 = 8000.
filtSeq.FourLeptonMassFilter.MaxMass2 = 14000000.
filtSeq.FourLeptonMassFilter.AllowElecMu = True
filtSeq.FourLeptonMassFilter.AllowSameCharge = True

evgenConfig.minevents = 50
evgenConfig.inputFilesPerJob = 3
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph VV aQGC'
evgenConfig.keywords+=['SM','ZZ','2jet','VBS']
#evgenConfig.inputfilecheck = stringy
evgenConfig.contact = ['Lailin Xu <lailin.xu@cern.ch>']