#!/bin/bash

# Make sure that if a command fails the script will continue
set +e

# Original directory
ORIGDIR=$PWD

# Directory for which to run athena
dir=$1
echo "INFO: Will run athena for $dir"

# Extract directory names
DSID=$(basename $dir)
DSIDxxx=$(dirname $dir)

# Move to DSID directory (actually 1 directory up since athena won't run if we go in the same dir...)
cd $DSIDxxx

# There should be only 1 file called log.generate.short in each of these directories
if [ ! -f $DSID/log.generate.short ] ; then
  echo "WARNING: No log.generate.short in $dir"
  echo "athena running will be skipped for this directory, however make sure that the requesters have tested the jO localy!!!"
  cd $ORIGDIR
  exit 0
  # The above solution is not ideal - only temporary until we enforce running athena for all new jO
  # exit 1
else
  echo "OK: log.generate.short found"
fi

# Check if the job is expected to last more than 1 hour
cpu=$(grep CPU $DSID/log.generate.short | awk '{print $8}')
if (( $(echo "$cpu > 1.0" | bc -l) )) ; then
  echo "ERROR: Job is expected to last more than 1h - time estimate: $cpu hours"
  echo "It is not possible to run this job in the CI. Contact the MC software coordinators"
  exit 1
else
  echo "OK: Job expected to last less than 1h - time estimate: $cpu hours"
fi
echo -e "\033[0m" # switch back terminal colour to black

# If external LHE is used skip athena in CI
inputGeneratorFile=$(grep inputGeneratorFile $DSID/log.generate.short)
if [[ ! -z $inputGeneratorFile ]] ; then
  echo "WARNING: jO in $dir uses an external LHE file"
  echo "CI cannot handle yet external LHE files, so athena running will be skipped for this directory. Make sure that the requesters have tested the jO localy!!!"
  cd $ORIGDIR
  exit 0
fi

# Get the release to use from the log.generate.short file
rel=$(grep 'using release' $DSID/log.generate.short | awk '{print $NF}')
relName=$(echo $rel | awk 'BEGIN {FS="-"} ; {print $1}')
relVer=$(echo $rel | awk 'BEGIN {FS="-"} ; {print $2}')

# Setup athena release
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
asetup $relVer,$relName

# Get ecmEnergy from the log.generate.short file
ecmEnergy=$(grep 'ecmEnergy =' $DSID/log.generate.short | awk '{print $NF}')
if [ -z $ecmEnergy ] ; then ecmEnergy=13000 ; fi

# Find number of events to run from jO
jOPath=$(ls $ORIGDIR/$DSIDxxx/$DSID/mc.*.py)
nEventsPerJob=$(python $ORIGDIR/scripts/jo_utils.py --parameter="nEventsPerJob" --jO=$jOPath)

# Number of events to run = max(1,0.01*nEventsPerJob)
nEvents=$(bc <<< "if (1-0.01*$nEventsPerJob >0) 1 else 0.01*$nEventsPerJob/1")
  
# Check if it's an LHE-only jO
if [[ $(grep LHEonly $DSID/log.generate.short | awk '{print $NF}') == "True" ]] ; then
  LHEonly=true
  nEvents=1
else
  LHEonly=false
fi

# Create a temporary directory from which we will run athena
mkdir tmp_$DSID
cd tmp_$DSID
TMP_DSID_DIR=$PWD

# If there is a GRID file then copy it to the directory where we run athena
GRIDfile=$(ls ../$DSID/*.GRID.tar.gz 2>/dev/null)
if [ ! -z "$GRIDfile" ] ; then
  link=$GRIDfile
  # We need to iterate here to resolve relative links
  relLink=0
  while $(test -L $link) ; do
    link=$(readlink $link)
    # Jump to the directory where the parent link is located
    cd $(dirname $link) 2> /dev/null
    let relLink=$relLink+1
    if [ $relLink -gt 10 ] ; then # This should never happen, but you never know...
      echo "ERROR: file $GRIDfile is a link that points back to itself"
      exit 1
    fi
  done
  # Go back to DSID directory where we started
  cd $TMP_DSID_DIR
  
  echo "INFO: GRID file $GRIDfile will be copied from $link"
  GRIDfilename=$(basename $link)
  # Remove the link
  rm -f $GRIDfile
  # And copy the actual file
  if [[ "$link" == "/eos/user"* ]] ; then
    HOST=root://eosuser.cern.ch
    xrdcp $HOST/$link ../$DSID/$GRIDfilename
  elif [[ "$link" == "/eos/atlas"*  ]] ; then
    HOST=root://eosatlas.cern.ch
    xrdcp $HOST/$link ../$DSID/$GRIDfilename
  else
    cp $link ../$DSID/$GRIDfilename
  fi
  
  if [ -f ../$DSID/$GRIDfilename ] ; then
    echo "OK: File successfully copied"
  else
    echo "ERROR: Copying GRID file failed"
    exit 1
  fi
fi

# Run athena
echo "Will now run Gen_tf with $nEvents events"
set -e # to catch any error from the execution of Gen_tf
if $LHEonly; then
  Gen_tf.py --ecmEnergy=$ecmEnergy \
  --jobConfig=../$DSID \
  --outputTXTFile=LHE.TXT.tar.gz \
  --maxEvents=$nEvents
else
  Gen_tf.py --ecmEnergy=$ecmEnergy \
  --jobConfig=../$DSID \
  --outputEVNTFile=EVNT.root \
  --maxEvents=$nEvents
fi
set +e # to avoid exiting from the script if any other setup command fails in the loop

# Update the number of jobs processed
let jobsProcessed=$jobsProcessed+1

# Rename log.generate output from transform to log.generate_ci
# which is uploaded as an artifact
if [ -f log.generate ] ; then
  mv log.generate ../$DSID/log.generate_ci
else
  echo "ERROR: no log.generate file produced"
  exit 1
fi

# Move to main directory
cd $ORIGDIR

exit 0

