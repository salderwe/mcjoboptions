import os, sys

# job templates
stages="""
stages:
  - run_athena
  - check_logParser
"""

athenaJob="""
#-----------------------------------------------------
# RUN ATHENA: CHILD JOB {_jobNumber}
#-----------------------------------------------------
run_athena_{_jobNumber}:
  stage: run_athena
  tags:
    - cvmfs
  # image built from ASG https://gitlab.cern.ch/atlas-sit/docker/tree/master/slc6-atlasos
  image: {_image}
  variables:
    ATLAS_LOCAL_ROOT_BASE: /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  artifacts:
    when: always
    paths:
      - "[0-9][0-9][0-9]xxx/**/log.generate_ci"
      - "[0-9][0-9][0-9]xxx/**/log.generate"
    expire_in: 1 week
  before_script:
    - echo ${{K8S_SECRET_SERVICE_PASSWORD}} | kinit ${{SERVICE_ACCOUNT}}@CERN.CH
  script:
    - ./scripts/run_athena.sh {_DSID}
"""

logParserJob="""
#-----------------------------------------------------
# CHECK OUTPUT: CHECK OUTPUT WITH LOGPARSER
#-----------------------------------------------------
check_logParser_{_jobNumber}:
  stage: check_logParser
  image:
    name: python:3.6.10 # For f-string support in logParser
    entrypoint: [""] # so that we start from bash and not from a python shell (default in image)
  needs: ["run_athena_{_jobNumber}"]
  script:
    - ./scripts/check_logParser.sh {_DSID}
"""

dummyJob="""
#-----------------------------------------------------
# DUMMY JOB
#-----------------------------------------------------
dummy:
  stage: run_athena
  script:
    - echo "No JO to run on"
"""

# Function to extract release to run on
def getRelease(DSID):
    if os.path.isfile('{DSIDdir}/log.generate.short'.format(DSIDdir=DSID)):
        with open('{DSIDdir}/log.generate.short'.format(DSIDdir=DSID), 'r') as file:
            for line in file.readlines():
                if "using release" in line:
                    return int(line.split()[-1].split('-')[-1].replace('.',''))
    else:
        return None

# Function to generate run_athena CI config on-the-fly
def populateConfig(jobNumber, DSID, rel):
    # Specify which image to use
    if rel is not None:
        if rel >= 21651:
            image='atlas/centos7-atlasos'
        else:
            image='atlas/slc6-atlasos'
    else:
        image=None
    
    # Write config to file
    with open('.run_athena.yml', 'a+') as file:
        # Pipeline stages
        file.write(stages)
        # CI jobs
        if DSID is not None and rel is not None:
            file.write(athenaJob.format(_jobNumber=jobNumber, _image=image, _DSID=DSID))
            file.write(logParserJob.format(_jobNumber=jobNumber, _DSID=DSID))
        else:
            file.write(dummyJob)

def main():
    # Get all directories in which jobOptions have been modified/added
    # Explanation: git diff-tree gets the files that have been added, modified, deleted etc
    # grep will look for DSID directories named like NNNxxx
    # awk will split the paths by slashes and will print the path except the last field which is the filename (NF="")
    # Then we only keep one such path (uniq) removing the trailing slash with sed
    command="git diff-tree --name-only -r origin/master..HEAD --diff-filter=AM | grep -E '[0-9]{3}xxx/[0-9]{6}/mc.*.py' | awk 'BEGIN {FS=OFS=\"/\"} {$NF=\"\" ; print $0}' | uniq | sed 's|/$||'"
    # Files will be a list containing strings like "700xxx/700001"
    files=list(filter(None,os.popen(command).read().strip().split("\n")))
    
    # Number of athena jobs to run
    try:
        njobs=int(sys.argv[1])
    except:
        print("You need to provide number of configs to create")
        sys.exit(1)
    
    # Create CI jobs
    if len(files) == 0: # If there are no jO files just create a dummy job that will succeed by default
        populateConfig(0, None, None)
    else:
        nRealJobs=0
        for i in range(1,njobs+1):
            # Generate athena and logParser jobs
            if i <= len(files):
                # Get release (in order to determine which image needs to be used)
                rel=getRelease(files[i-1])
                # rel can be none if there is no log.generate.short file
                if rel is not None:
                    # Populate the job config
                    print("INFO: generate CI config for {_DSID} with release {_rel}".format(_DSID=files[i-1], _rel=rel))
                    populateConfig(files[i-1].split("/")[1], files[i-1], rel)
                    nRealJobs += 1
        # If no log.generate.short files have been found, even if there are jO committed
        # there would be no real athena job created so we would need to create a dummy one
        if nRealJobs == 0:
            populateConfig(0, None, None)
        
if __name__== "__main__":
    main()

