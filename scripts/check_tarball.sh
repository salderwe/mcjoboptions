#!/bin/bash

# Make sure that if a command fails the script will continue
set +e

echo "Find links added in the last commit..."
# Find number of files that have been:
# Added(A), Copied (C), Deleted (D), Modified (M), Renamed (R), have their type
# (i.e. regular file, symlink, submodule, …​) changed (T), are Unmerged (U),
# are Unknown (X), or have had their pairing Broken (B).
changed=($(git diff-tree --name-only -r origin/master..HEAD --diff-filter=AMCRTUXB | grep 'GRID.tar.gz'))

if (( ${#changed[@]} == 0 )) ; then
  echo "No GRID.tar.gz files added/modified since last commit"
  exit 0
fi

basedir=$PWD
fail=false

# Loop over all links
for file in "${changed[@]}" ; do

  echo "Checking file: $file..."

  # Go into directory where the GRID file is
  dir=$(dirname $file)
  cd $dir

  # Check where the file is located
  link=$(basename $file)
  # We need to iterate here to resolve relative links
  relLink=0
  while $(test -L $link) ; do
    link=$(readlink $link)
    # Jump to the directory where the parent link is located
    cd $(dirname $link) 2> /dev/null
    let relLink=$relLink+1
    if [ $relLink -gt 10 ] ; then # This should never happen, but you never know...
      echo "ERROR: file $file is a link that points back to itself"
      exit 1
    fi
  done
  
  if [[ "$link" == "/eos/user"* ]] ; then
    HOST=root://eosuser.cern.ch
  elif [[ "$link" == "/eos/atlas"*  ]] ; then
    HOST=root://eosatlas.cern.ch
  elif [[ "$link" == "/cvmfs/atlas.cern.ch/repo/sw/Generators/MCJobOptions/"*  ]] ; then
    echo "OK: The link is pointing to /cvmfs. Nothing more to check."
    exit 0
  else
    echo "The link is not pointing to /eos/atlas, /eos/user or /cvmfs"
    readlink $file
    echo "Please transfer the file to either /eos/atlas or /eos/user or if you are using an existing GRID file already registered on cvmfs update the link to point to the correct cvmfs location."
    exit 1
  fi
  
  # Because of the way eos works we need to get the parent directory containing the file to which the link points
  dirlink=$(dirname $link)

  # Check if atlcvmfs is in the ACL list
  eos $HOST
  access=$(eos $HOST acl -l --sys $dirlink 2>/dev/null)
  echo "The access rights are set to: $access"
  
  # mcgensvc readability test
  if [[ "$access" == *"mcgensvc:r"* ]] ; then
    echo "OK: mcgensvc can read $dirlink"
  else 
    echo "ERROR: mcgensvc cannot read $dirlink"
    fail=true
  fi
  
  # atlcvmfs readability test
  if [[ "$access" == *"atlcvmfs:r"* ]] ; then
    echo "OK: atlcvmfs can read $dirlink"
  else
    echo "ERROR: atlcvmfs cannot read $dirlink"
    fail=true
  fi

  # Check if the file is readable by all
  access=$(eos $HOST ls -l $link 2>/dev/null | awk '{print $1}')
  if [[ $access =~ ^.r..r..r.. ]] ; then
    echo "OK: file $link is readable by all users"
  else
    echo "ERROR: file $link is not readable by all users. The permissions are set to: $access"
    fail=true
  fi
  
  # Check number of files in gridpack
  xrdcp $HOST/$link ./gridpack.tar.gz
  # Count number of regular files (no links or directories)
  nfiles=$(tar tzvf gridpack.tar.gz | grep -c '^-')
  if (( nfiles < 80000 )) ; then
    echo "OK: gridpack $link contains less than 80k files (nfiles=$nfiles)"
  else
    echo "ERROR: gridpack $link contains more than 80k files (nfiles=$nfiles)."
    fail=true
  fi
  rm -f gridpack.tar.gz
  
  cd $basedir
done

if $fail ; then
  exit 1
fi

exit 0
