#!/bin/bash

# Make sure that if a command fails the script will continue
set +e

# Directory for which to run athena
dir=$1
echo "INFO: Will run logParser for $dir"

# There should be only 1 file called log.generate.short in each of the DSID directory
# This should have been uploaded by the user (running scripts/commit_new_dsid.sh locally)
if [ ! -f $dir/log.generate.short ] ; then
  echo "WARNING: No log.generate.short in $dir"
  echo "athena running was skipped for this directory. Make sure that the requesters have tested the jO localy!!!"
  exit 0
else
  echo "OK: log.generate.short found"
fi

# There should be only 1 file called log.generate_ci in each of these directories
# This should have been produced as an artifact by the run_athena CI job
if [ ! -f $dir/log.generate_ci ] ; then
  # log.generate_ci might be missing if athena running has been skipped in the previous iteration
  # due to the presence of external LHE or because of an unsupported release
  inputGeneratorFile=$(grep inputGeneratorFile $dir/log.generate.short)
  minorRel=$(grep 'using release' $dir/log.generate.short | awk '{print $NF}' | awk 'BEGIN {FS="-"} ; {print $2}' | awk 'BEGIN {FS="."} ; {print $3}')
  if [[ ! -z $inputGeneratorFile ]] ; then
    echo "INFO: athena running has been skipped in $dir since jO uses an external LHE file"
    exit 0
  else
    echo "ERROR: No log.generate_ci in $dir."
    echo "The run_athena CI job must have failed. Please check output of run_athena CI job."
    exit 1
  fi
else
  echo "OK: log.generate_ci found"
fi

echo "Running: python scripts/logParser.py -i $dir/log.generate_ci -j $joFile -c -u ..."

# Check output
joFile=$(ls $dir/mc.*.py)
python scripts/logParser.py -i $dir/log.generate_ci -j $joFile -c -u | tee log.generate_ci_out
if [ "$?" != "0" ] ; then
  echo "ERROR: logParser execution failed. This usually implies that $dir/log.generate_ci is malformatted or there is a run-time error from logParser."
  echo "Check the output of the previous CI job and the content of log.generate_ci (in the artifacts of this job)."
  exit 1
fi

# If no error: remove log.generate file from repository
errors=$(grep -E 'Errors.*Warnings' log.generate_ci_out | awk '{print $3}')
rm -f log.generate_ci_out
if (( errors > 0 )) ; then
  echo "ERROR: $dir/log.generate_ci contains errors"
  exit 1
else
  echo "OK: logParser found no errors."
fi

exit 0
