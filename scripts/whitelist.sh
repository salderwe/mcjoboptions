#!/bin/bash

# Function to check if a file is in the whitelist of files that are allowed to be added in a commit
checkWhiteList() {

    # Rules for DSID DIR
    if $dryrun ; then # this is only true when checkWhitelist is called from the commit script
      DIR=".*"
    else
      DIR="[0-9]{3}xxx/[0-9]{6}"
    fi
    
    # The path to the file being tested
    path=$(echo $1)
    top2Dirs=$(dirname $path | awk -F'/' '{printf "%s/%s" , $1, $2}')
    
    if [ -f $1 -a ! -L $1 ] ; then
       # Regular file checks
        if [[ ($path =~ $DIR/.*\.py) ||
              ($path =~ $DIR/.*\.f) ||
              # dat files have to live in the top-level jO directory
              (($path =~ $DIR/.*\.dat) && !(($top2Dirs =~ [0-9]{3}xxx/[0-9]{6} && $path =~ [0-9]{3}xxx/[0-9]{6}/.*/.*\.dat) || ( !($top2Dirs =~ [0-9]{3}xxx/[0-9]{6}) && $path =~ $DIR/.*/.*\.dat))) ||
              ($path =~ $DIR/pdgid_extras.txt) ||
              ($path =~ $DIR/powheg.input) ||
              ($path =~ $DIR/log\.generate\.short) ||
              ($path =~ $DIR/MadGraphControl/.*\.py) ||
              ($path =~ $DIR/PowhegControl/.*\.py) ||
              ($path =~ $DIR/Herwig7_i/.*\.py) ||
              ($path =~ $DIR/Pythia8_i/.*\.py) ||
              ($path =~ $DIR/Sherpa_i/.*\.py) ]] ; then
            return 0 #true
        else
            return 1 #false
        fi
    elif [ -L $1 ] ; then
        # Link checks
        if [[ ($path =~ $DIR/mc_.*TeV\..*\.GRID\.tar\.gz) ||
              ($path =~ $DIR/.*\.py) ||
              ($path =~ $DIR/.*\.f) ||
              # dat files have to live in the top-level jO directory
              (($path =~ $DIR/.*\.dat) && !(($top2Dirs =~ [0-9]{3}xxx/[0-9]{6} && $path =~ [0-9]{3}xxx/[0-9]{6}/.*/.*\.dat) || ( !($top2Dirs =~ [0-9]{3}xxx/[0-9]{6}) && $path =~ $DIR/.*/.*\.dat))) ||
              ($path =~ $DIR/pdgid_extras.txt) ||
              ($path =~ $DIR/powheg.input) ||
              ($path =~ $DIR/MadGraphControl/.*\.py) ||
              ($path =~ $DIR/PowhegControl/.*\.py) ||
              ($path =~ $DIR/Herwig7_i/.*\.py) ||
              ($path =~ $DIR/Pythia8_i/.*\.py) ||
              ($path =~ $DIR/Sherpa_i/.*\.py) ]] ; then
            return 0 #true
        else
            return 1 #false
        fi
    else
        echo "Unknown file/filetype: $path"
        return 1
    fi
}

