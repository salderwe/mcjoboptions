import argparse, os

def _parse(dsids):
    """A function to determine consecutive DSIDs and work out ranges.
    Takes as input an array of string-DSIDs.
    Prints out a string of comma-separated individual DSIDs / DSID ranges,
    e.g. 123456,600000,600001,600002,987654 -> 123456,600000-600002,987654"""
    from itertools import groupby
    from operator import itemgetter
    dsids = [ int(d) for d in dsids ] # turn strings to integers
    groups = [ list(map(itemgetter(1), l)) for k, l in groupby(enumerate(dsids), lambda item: item[0]-item[1]) ]
    # "groups" is an array of arrays. The inner arrays contain at least one DSID. 
    # If there's more than one DSID in an array, they must be consecutive, i.e. form a range.
    print( ','.join([ '%i-%i' % (g[0], g[-1]) if len(g) > 1 else str(g[0]) for g in groups ]) )

# A function to read a parameter from a jO file
# Taken from https://github.com/retmas-dv/deftcore/blob/master/taskengine/taskdef.py#L289
# ignore_case is set to False by default unlike ProdSys
def _read_param_from_jo(jo, names, ignore_case=False):
    value = None
    if ignore_case:
        names = [name.lower() for name in names]
        jo = jo.lower()
    if any(p in jo for p in names):
        for line in jo.splitlines():
            if any(p in line for p in names):
                try:
                    if '#' in line:
                        line = line[:line.find('#')]
                    # The int conversion will throw an exception if nEventsPerJob is not a number
                    value = int(line.replace(' ', '').split('=')[-1])
                    break
                except Exception as ex:
                    pass
    return value

# A function to read a parameter from a jO file using a dictionary
# The difference with the previous one is that it follows
# See https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/issues/98
def _read_param_from_jo_withDict(jo, type, param):
    locals = {type: argparse.Namespace()}
    for line in jo.splitlines():
        if "os.system" in line: continue # for security
        try:
            exec(line, {}, locals)
        except:
            pass
    return getattr(locals[type], param) if hasattr(locals[type], param) else None

# A function to check if environment variables are set in jO file
def _env_set(jo, param):
    # Get original environment variables
    orig_env = dict(os.environ)
    # Clear environment variables
    os.environ.clear()
    # dictionary to store environment variables
    locals = dict()
    # Execute jO
    for line in jo.splitlines():
        try:
            exec(line.strip(), globals(), locals)
        except:
            pass
        # Get value of environment variable - if it has been set break
        if os.environ.get(param) is not None:
             break
    # Restore environment
    os.environ.update(orig_env)
    if os.environ.get(param) is not None:
        return [True, os.environ.get(param)]
    else:
        return [False, None] 

### Steering ###
if __name__ == "__main__":
    import argparse
    # Argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument("DSIDs", nargs="*")
    parser.add_argument("--jO", dest="jOFile", default=None)
    parser.add_argument("--parameter", dest="parameter", action="store", default=None)
    parser.add_argument("--dsid-parser", dest="dsid_parser", action="store_true", default=False)
    args = parser.parse_args()
    
    if args.dsid_parser and len(args.DSIDs) > 0:
        # Check if list of DSIDs have been supplied
        _parse(args.DSIDs)
    elif args.jOFile is None:
        # Check if jO file has been defined
        raise Exception("ERROR: --jO argument not specified")
    else:
        # Read jO contents
        with open(args.jOFile, 'r') as jofile:
            job_options_file_content = jofile.read()
    
    # Extract different parameters
    if args.parameter == "nEventsPerJob":
        nEventsPerJob=_read_param_from_jo(job_options_file_content, ['evgenConfig.nEventsPerJob'])
        if not nEventsPerJob:
            nEventsPerJob=10000
        print(nEventsPerJob)
    elif args.parameter == "minevents":
        print(_read_param_from_jo(job_options_file_content, ['evgenConfig.minevents']))
