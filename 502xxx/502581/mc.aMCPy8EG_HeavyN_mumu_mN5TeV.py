import subprocess
retcode = subprocess.Popen(['get_files', '-jo', 'HeavyNCommon.py'])
if retcode.wait() != 0:
    raise IOError('could not locate HeavyNCommon.py')

import HeavyNCommon

HeavyNCommon.process = HeavyNCommon.available_processes['mumuchannel']
HeavyNCommon.parameters_paramcard['mass']['mN2'] = 5000
HeavyNCommon.parameters_paramcard['numixing']['VmuN1'] = 1
HeavyNCommon.parameters_paramcard['numixing']['VmuN2'] = 1
HeavyNCommon.parameters_paramcard['numixing']['VmuN3'] = 1

HeavyNCommon.run_evgen(runArgs, evgenConfig, opts)
